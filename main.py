import requests
import time
import os
import json

token = None


def color_text(value):
    if value > 0:
        # Green color for positive values
        return f'\033[92m{value}\033[0m'  # 92 is the ANSI code for green
    elif value < 0:
        # Red color for negative values
        return f'\033[91m{value}\033[0m'  # 91 is the ANSI code for red
    else:
        return str(value)


class FIOWrapper:
    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password
        self.token = None

        if self.getAuthToken():
            print("Token file found and loaded.")
            print("Authenticating...")

        else:
            print("Token not found, attempting to obtain.")
            self.Login()

        self.Authenticate()

    def getAuthToken(self):
        if os.path.exists('token.json'):
            with open('token.json', 'r') as token_file:
                token_json = json.load(token_file)
                self.token = token_json["AuthToken"]
                return True
            print("Token file found and loaded.")
        else:
            return False

    def Login(self):
        url = "https://rest.fnar.net/auth/login"
        headers = {
            "accept": "application/json",
            "Content-Type": "application/json"
        }
        data = {
            "UserName": self.username,
            "Password": self.password
        }
        response = requests.post(url, headers=headers, json=data)

        if response.status_code == 200:
            # Request was successful, and you can access the response data like this:
            response_data = response.json()
            self.token = response_data["AuthToken"]
            with open('token.json', "w") as token_file:
                json.dump(response_data, token_file)

        else:
            # Request failed, and you can handle errors here
            print("Request failed with status code:", response.status_code)

    def Authenticate(self):
        url = "https://rest.fnar.net/auth"
        headers = {
            "accept": "application/json",
            "Authorization": self.token
        }

        response = requests.get(url, headers=headers)
        print(response.text)
        if response.status_code == 200:
            # Request was successful, and you can access the response data like this:
            if response.text == self.username.upper():
                print("Authentication successful")

        if response.status_code == 401:
            print("Unauthorized, failed authentication")
        else:
            # Request failed, and you can handle errors here
            print("Request failed with status code:", response.status_code)


def CommodityGet(commodity, station):
    url = "https://rest.fnar.net/exchange/{}.{}".format(commodity, station)
    headers = {
        "accept": "application/json",
    }
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        # Request was successful, and you can access the response data like this:
        return response.json()
    else:
        # Request failed, and you can handle errors here
        print("Request failed with status code:", response.status_code)


def GetAllRecipes(token):
    file_path = './recipes.json'
    if not os.path.exists(file_path):
        print("Recipe file not found, downloading.")
        url = "https://rest.fnar.net/recipes/allrecipes"
        headers = {
            "accept": "application/json",
            "Authorization": token
        }
        response = requests.get(url, headers=headers)
        response_data = response.json()
        with open(file_path, "w") as json_file:
            json.dump(response_data, json_file)

        if response.status_code == 200:
            # Request was successful, and you can access the response data like this:
            return response.text
        else:
            # Request failed, and you can handle errors here
            print("Request failed with status code:", response.status_code)
    else:
        print("Found recipe file, using local : " + file_path)


def RecipeReader():
    with open('recipes.json', 'r') as recipe_file:
        return json.load(recipe_file)


def RecipesByBuilding(recipes, building):
    filtered_data = [item for item in recipes if item['BuildingTicker'] == building]
    return filtered_data


def ConsumptionRatebyBuilding(building_recipes):
    for recipe in building_recipes:
        print("For recipe: " + recipe['RecipeName'])
        input_amount = None
        input_type = None
        time_hours = int(recipe['TimeMs']) / 1000 / 60 / 60

        for input in recipe['Inputs']:
            input_amount = int(input['Amount'])
            input_type = input['Ticker']

            consumption_rate = (input_amount / time_hours)

            print(f"For input type {input_type}, it burns {consumption_rate}p/h")


def getCX_prices():
    if os.path.exists('data/all_cx_prices.json'):
        with open('./data/all_cx_prices.json', 'r') as cx_price_file:
            return json.load(cx_price_file)
    else:
        print("Recipe file not found, downloading.")
        url = "https://rest.fnar.net/rain/prices"
        headers = {
            "accept": "application/json"
        }
        response = requests.get(url, headers=headers)
        response_data = response.json()
        with open('./data/all_cx_prices.json', "w") as json_file:
            json.dump(response_data, json_file)

        with open('./data/all_cx_prices.metadata', "w") as metadata:
            metadata.write(response.headers._store['date'][1])
        return False


def BuildingProfitAnalysisByRecipe(building_recipes):
    cx_prices = None
    with open('./data/all_cx_prices.json', 'r') as recipe_file:
        cx_prices = json.load(recipe_file)

    ticker_dict = {item['Ticker']: item for item in cx_prices}

    for recipe in building_recipes:
        print("For recipe: " + recipe['RecipeName'])
        input_amount = None
        input_type = None

        output_amount = None
        output_type = None
        input_cost = None
        output_cost = None
        input_cost_total = 0
        time_hours = int(recipe['TimeMs']) / 1000 / 60 / 60

        for input in recipe['Inputs']:
            input_amount = input['Amount']
            input_type = input['Ticker']
            input_price = None

            if input_type in ticker_dict:
                if input_type == 'LST':
                    input_price = 0
                else:
                    cx_mat_dict_by_ticker = ticker_dict[input_type]
                    input_price = cx_mat_dict_by_ticker['NC1-AskPrice']

            input_cost = input_price * input_amount
            input_cost_total = input_cost_total + int(input_cost)

        for output in recipe['Outputs']:
            output_amount = output['Amount']
            output_type = output['Ticker']
            output_price = None

            if output_type in ticker_dict:
                cx_mat_dict_by_ticker = ticker_dict[output_type]
                output_price = cx_mat_dict_by_ticker['NC1-BidPrice']
                if output_price is None:
                    output_price = cx_mat_dict_by_ticker['NC1-Average']

            output_cost = output_price * output_amount

            # print("Output type: " + str(output_type) + " Output cost : " + str(output_cost))

        profit = int(output_cost - input_cost_total)
        prof_p_h = int(profit / time_hours)
        print("Output - Input = " + str(output_cost) + " - " + str(input_cost_total) + " = " + str(profit))
        # print("time_hours = " + str(time_hours))
        print(f"Profit/TimeHours= {color_text(prof_p_h)}")


FIO = FIOWrapper(username="", password="")  # Saves token for some API calls that would require it.

GetAllRecipes(FIO.token) # Gets recipes JSON from remote
Recipes = RecipeReader() # Loads recipes


PP1 = RecipesByBuilding(Recipes, "PP1")  # Gathers recipes by that building type

Consoom = ConsumptionRatebyBuilding(PP1)
# Gets consumption rate of raw resources by building's recipe, per hour
BuildingProfitAnalysisByRecipe(PP1)
# Prints out a list of profitability by recipe type, comparing cost of raw goods versus end product's market cost.

test = getCX_prices()

# Get all ticker data.
# Find largest diffierntial
# Calculate distance costs between stations
# Account in costs to fly to differential
# Find max capacity flight with profit
